# Modo insertar
Como su nombre nos indica, permite realizar inserciones en el texto.  
Existen diferentes teclas que nos ayudan a mover el cursor y entrar en este modo partiendo del *modo normal*.

| Tecla | Descripción |
|-------|-------------|
|   i   | Pone el cursor antes de la letra |
|   a   | Pone el cursor después de la letra (after) |
|   A   | Pone el cursor al final de la línea |
|   o   | Crea una línea nueva debajo de la actual |
|   O   | Crea una línea nueva sobre la actual |

Aunado a lo anterior, podemos hacer modificaciones con el texto antes de entrar al modo insert.

| Combinación | Descripción |
|-------------|-------------|
| cw          | (change word) Remplaza la palabra siguiente a partir del cursor |
| ciw         | Remplaza la palabra sin tener que estar el cursor al inicio de la misma |
