# Vim
Vim es editor de código que podemos usar desde la terminal. Existe una versión con esteroides llamada neovim que nos puede ayudara  tener una mejor experiencia con ayuda de plugins.


---
## Modos

Los modos nos permiten realizar diferentes acciones y cambiar el contexto de trabajo.

 - [Modo normal](./NormalMode.md)
 - [Modo insertar](./InsertMode.md)
