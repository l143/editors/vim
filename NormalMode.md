# Modo normal  

En el modo normal no podemos insertar texto en el archivo, sin embargo podemos realizar otras acciones como:

- Movernos rápidamente a través del texto.
- Navegar por los archivos y las definiciones.
- Guardar el archivo
- Salir del editor



## Movimientos

---

Para movernos a lo largo del texto existen algunas teclas que nos serán de mucha ayuda.

> Movimiento en las 4 direcciones.  

```
        ______
       |      |
 ______| ↑, k |______
|      |______|      |
| ←,h  |      |  →,l |
|   b  |______|  w,e |
|______|      |______|
       | ↓, j |      
       |______|

```

Teclas para movimientos de posición unitaria:  

 - h, k, l, j

Teclas para movimientos de posición por palabra:  
 - **b**: Mueve el cursor al inicio de la palabra anterior
 - **w**: Mueve el cursor al inicio de la siguiente palabra
 - **e**: Mueve el cursor al final de la siguiente palabra

---
## Operadores
Los operadores nos ayudarán a realizar algunas acciones.

| Tecla  | Descripción |
|--------|-------------|
|   u    | Deshacer    |
| Ctrl+r | Rehacer     |
|   dd   | Elimina toda la  línea del cursor |
|   yy   | Copia toda la línea del cursor |
|   cc   | Corta la línea del cursor |
|   P    | Pega la línea previamente cortada o copiada en la línea superior al cursor |
|   p    | Pega la línea previamente cortada o copiada en la línea inferior al cursor |

---

## Modificaciones
Podemos hacer algunas modificaciones sin entrar al modo insertar.

|Tecla    |Descripción|
|---------|-----------|
|  x      | Funciona como la tecla supr |
| dw, de  | Elimina la siguiente palabra |
| db      | Elimina la palabra anterior |
| d$      | Elimina desde la posición del cursor hasta el **final** de la línea |
| d^      | Elimina desde la posición del cursor hasta el **inicio** final de la línea |
| r	  | Reemplaza el caracter en dónde se encuentra el cursor |
| :s/\<texto>/\<reemplazo> | **Reemplaza una** ocurrencia en la línea en cuestion, si queremos que reemplaze todas agregamos un /g al final |
| :%s/\<texto>/\<reemplazo>/g | **Reemplaza todas** las cocurrencias dentro del archivo, si queremos que nos pregunte una por una, agregamos una 'c' después de la 'g' final|

Los números pueden ser aplicados también.  
Es decir si quieremos realizar alguna acción de las de la tabla n veces, podemos presionar la cantidad de veces en números (1, 2, 3) seguido de la combinación de teclas correspondiente.

---

## Navegar entre archivos y definiciones.
Vim nos permite movernos a través del documento sin entrar al modo de inserción.  
Para esto nos ofrece algunas combinaciones de teclas.  

| Combinación | Descripción |
|-------------|-------------|
|   ^\|0      | Lleva el cursor al inicio de la línea |
|     $       | Lleva el cursor al final de la línea |
|     gg      | Lleva el cursor al inicio del documento |
|  CTRL + g   | Lleva el cursor al final del documento |
|     gd      | Cuando se usa sobre una función, nos lleva a la definición o importación en el mismo archivo |
|     gf      | Cuando se usa sobre una referencia a un archivo, abre el archivo en cuestión |


Lo anterior crea algo conocido como historial de navegación ó buffer, el cual podremos explorar de la siguiente forma.

| Combinación | Descripción |
|-------------|-------------|
|  ctrl + o   | Nos lleva un paso atrás en el historial |
|  ctrl + i   | Nos lleva un paso adelante en el historial |

## Búsqueda
Para buscar podemos usar los siguientes caracteres desde el modo normal
| Caracter | Descripción |
|--|--|
| /\<textoABuscar> | Busca a partir de dónde esta el cursor hacia adelante |
| ?\<textoABuscar> | Busca a aprtir del cursor hacia atrás |
| n               | Nos mueve al **siguiente** elemento de los resultados resaltados en una búsqueda |
| N               | Nos mueve al elemento **anterior** de los resultados resaltados en una búsqueda |
